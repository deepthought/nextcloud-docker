# readme

synopsis: install docker-nextcloud with script using docker-compose. this setup can speed up spinning up nextcloud instances rapidly for testing, or to just help make an install easier. this is important because it seems like the nextcloud code has a critical flaw that creates serious database errors with a large number of files that makes it non-production ready. I want to see nextcloud succeed, but I don't think it will if these issues are not addressed well.

see other script for installing on freenas: https://github.com/ak1n/freenas-iocage-nextcloud

## what is my purpose?
* script to setup a nextcloud docker install
* one configuration file - hopefully no other configuration required
* configuration options:
  * mysql/mariadb or postgres
  * nocert (http) or letsencrypt cert (https)
* default settings:
  * cache: APCu for local, redis for locking
  * file-locking w redis enabled
  * fpm (rather than apache)
* increase consistency across installs to reduce variables in debugging problems with nextcloud, and increase general reliability if these bugs can be addressed
* helpful references used in creating script:
  * nextcloud-docker (note errors in postgres configs): https://github.com/nextcloud/docker/tree/master/.examples/
  * other freenas script author: https://github.com/danb35/freenas-iocage-nextcloud

## use: show me what you got
* git clone the repo
* copy nextcloud-config.sh.sample to nextcloud-config.sh
* edit nextcloud-config.sh to:
  * set your IP address (LOCAL_IP)
  * set one of the certs to 1: either nocert or letsencrypt
    * add letsencrypt info if enabled
  * set one of the db options to 1: either mysql/mariadb or postgres
  * set letsencrypt variables if using certs: domain, email
  * if needed external ports for http & https can be changed
* execute `./setup.sh` with administrator access
* for testing or reset, following options also available:
  * -r: will reset the docker-compose install - **including deleting created volumes**, and rerun the script
    * i.e. includes `docker-compose down -v` (to reset without deleting volumes exclude the `-v` flag & use the docker command)
  * -s: setup only, without starting docker compose
  * -d: docker down. like reset but without recalling the script
* the script saves generated passwords in "passwords_to_save" folder in git root (so don't leave them sitting around in clear-text)

## but what is the purpose of all this? nextcloud file lock bugs
* nextcloud appears very broken - after relying on it for years on freenas suddenly I can't sync/upload due to bizarre lock errors
* at first I thought it was my fault but the lock errors seem replicable with new installs using recommended parameters
* suspicion thus drawn towards core code problems in server and/or nextcloud client (would love to be wrong)
* after spawning up ~10 freenas nextcloud servers without being able to resolve, found that using docker installs also afflicted by similar but different lock problems despite optimization tweaks
* using this script makes it easier/faster to do test installs and then try syncing real data
* hopefully this script is obsolete soon re nextcloud getting stable - if these lock issues aren't resolve (and they continue to not seem my fault) I may be looking for another storage/sync platform and recommending others do the same
* a related note: the recommended example files for postgres at nextcloud-docker **do not work** as written

## test dataset & protocol
* on server: antergos w nextcloud 14 via docker
  * download `NEW: Open Images Challenge 2018 Test Set - test_challenge.zip | 9.7 GB` from: https://www.figure-eight.com/dataset/open-images-annotated-with-bounding-boxes/
  * copy the zip into the ~/Nextcloud directory & unzip it there
  * leave the extracted data in a subfolder (i.e. subfolder within ~/Nextcloud) **and the zip** in the Nextcloud directory
  * run the script to spin up the server/containers
  * create a non-admin 1st user
* client machine: kde neon w nextcloud client 2.3.3
  * ensure no prior `._sync...` or dot Nextcloud log files in ~/Nextcloud folder, nor other Nextcloud accounts connected
  * run nextcloud desktop client program & sync to new server instance
* log errors

## non-error results
* after much tweaking the following 4 modes were verified as leading to nextcloud installs (functional aside from db et al. described errors): mariadb/mysql or postgres, with nocert or letsencrypt

## what errors? they include...

### with test dataset as per above
* fatal lock errors: the data usually starts to sync without issues, then a bit beyond halfway "...file is locked" errors start appearing in the logs that appear to significantly slow down the sync transfer
   * `Fatal	webdav	OCA\DAV\Connector\Sabre\Exception\FileLocked: "..." is locked`
   * this replicated in all testing conditions: mariadb & postgres, nocert & letsencrypt
* gateway timeout errors appear when syncing the larger zip file
  * `||test_challenge.zip|INST_NEW|Up|1540829583||10404183024||2|Error transferring http://IP_REDACTED/remote.php/dav/uploads/USER_REDACTED/4062376235/.file - server replied: Gateway Time-out|504|0|0|||INST_NONE|`
  * replicated in both no-cert conditions, but not in letsencrypt runs

### non-test data: auditing your files - get your shit together
* in addition to the db errors I also acquired some related to elements of my files that I was trying to upload
* permissions: ensure you have access to all the files you're trying to upload
  * see if you're denied access to any of your folders: `ls -alR ~/Nextcloud | grep '^d' | egrep -v '(\.)|(\..)' | grep denied`
  * find owner:group values that don't match current user:group (may not work on all systems/groups-setups): `ls -alR ~/Nextcloud | grep '^d' | egrep -v '(\.)|(\..)' | egrep -v "$(whoami) $(id -g -n)"`
  * look for root ownership (may have root in some filenames): `ls -alR ~/Nextcloud | grep '^d' | egrep -v '(\.)|(\..)' | grep root`
  * if can change ownership without affecting other parts of your system: `sudo chown -R (your username) ~/Nextcloud`
* unusual characters in filenames: found a filename from a PCB design program that had % in the filename that elicited errors from the nextcloud client

### with prior less-structured testing

##### configuration per nextcloud recs
* redis enabled w explicit filelocking, and db/redis parameters tweaked as per above
* on first-user sync the logs spammed with locked errors:
```
Fatal	webdav	OCA\DAV\Connector\Sabre\Exception\FileLocked: "..." is locked
```

##### redis enabled & default parameters (sans tweaks)
```
  Sabre\DAV\Exception: An exception occurred while executing 'INSERT INTO `filecache` (`mimepart`,`mimetype`,`mtime`,`size`,`etag`,`storage_mtime`,`permissions`,`parent`,`checksum`,`path_hash`,`path`,`name`,`storage`) SELECT ?,?,?,?,?,?,?,?,?,?,?,?,? FROM `filecache` WHERE `storage` = ? AND `path_hash` = ? HAVING COUNT(*) = 0' with params [...]: SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry '...' for key 'fs_storage_path_hash'
```

##### redis disabled (removed from config memcache.locking...Redis & subsequent redis block, and reddit php config)
```
Error transferring... - server replied: Internal Server Error (An exception occurred while executing 'INSERT INTO `file_locks` (`key`,`lock`,`ttl`) SELECT ?,?,? FROM `file_locks` WHERE `key` = ? HAVING COUNT(*) = 0' with params...
```
  * very slow but would sync with many of above

#### freenas server / arch client setup
* Deadlock errors - tried many tweaks w/o resolution
* see configuration script: https://github.com/ak1n/freenas-iocage-nextcloud
```
Fatal	webdav	Doctrine\DBAL\Exception\DriverException: An exception occurred while executing 'INSERT INTO `oc_filecache` (`mimepart`,`mimetype`,`mtime`,`size`,`etag`,`storage_mtime`,`permissions`,`parent`,`checksum`,`path_hash`,`path`,`name`,`storage`) SELECT ?,?,?,?,?,?,?,?,?,?,?,?,? FROM `oc_filecache` WHERE `storage` = ? AND `path_hash` = ? HAVING COUNT(*) = 0' with params [STUFF]: SQLSTATE[40001]: Serialization failure: 1213 Deadlock found when trying to get lock; try restarting transaction
```
* see https://github.com/nextcloud/server/issues/6160

### nextcloud issues probably related to these lock errors
* https://github.com/nextcloud/server/issues/6160
* https://github.com/nextcloud/server/issues/6899
* https://github.com/nextcloud/server/issues/9305
* https://github.com/nextcloud/server/pull/10116
* https://github.com/nextcloud/server/issues/9001
* https://github.com/nextcloud/server/issues/8723
* https://github.com/nextcloud/server/issues/8239
* https://github.com/owncloud/core/issues/26980

## errors in perspective
* many comments/replies blame upstream database like mariadb - **except that they occur with postgres too** so this should not be the case unless there is evidence it can be traced to a shared dependence or something of the like
* many user replies mention that some minor tweak resolves the error, and this has led to perception of resolution when this is extraordinarily unlikely without further information about the dataset
  * e.g. if the initial errors came up with a large sync, but then not after when smaller uploads are occurring then the bugs are merely dormant not resolved
  * unless re-syncing from scratch with clear test dataset is attempted, these errors are probably dormant rather than resolved
* **if** the errors were to resolve with this aforementioned dataset, verification of resolution should be checked with other large datasets rather than mark resolution
  * **Note**: my actual prior Nextcloud folder seemed to generate more errors than this dataset
  * it could be improper lock/file validation could only lead to more significant errors with e.g. similar/same filenames
* the freenas errors were different and seemingly more serious, but I am not going to even try to debug them until things are functioning smoothly with docker

## reference
* if issues stopping with docker-compose - stop with docker:
  * find containers w `docker ps -a`
  * remove relevant containers via `docker rm id1 id2 etc`.
  * then prune volumes w `docker volumes prune`
* shell in nextcloud container if named nextcloud-docker_app_1:
  * `docker attach nextcloud-docker_app_1`
  * `docker exec -it nextcloud-docker_app_1 bash`
* from within container shells:
  * app/nextcloud:
    * config default: `/var/www/html/config/config.php`
    * data default: `/var/www/html/data`
  * db/mysql:
    * my.cnf et al. cnf files: `/etc/mysql`
    * view cnf outside docker: `sudo docker exec -it nextcloud-docker_db_1 cat /etc/mysql/my.cnf | less`
  * db/postgres:
    * CLI from outside docker: `sudo docker exec -it nextcloud-docker_db_1 psql -U nextcloud`
* redis CLI: `sudo docker exec -it nextcloud-docker_redis_1 redis-cli`
* running occ from outside e.g.: `docker exec --user www-data nextcloud-docker_app_1 php occ files:scan --all`
  * note: this command should NOT be a proposed resolution when starting with a new db/installation
* if you want to nano within a container shell: `apt update; apt install nano -y`
* to use mysql command line:
  * `docker exec -it nextcloud-docker_db_1 mysql -u root -p` & input password from initial script run
  * to verify config parameters via mysql CLI, e.g.: `show variables like '%innodb_io_capacity%';`

## halp
* if you have any recommendations regarding the above lock issues, or for the script, they would be much appreciated
* I almost certainly have missed mistakes in the above setup & code, and drawing attention to these to fix them would be appreciated
