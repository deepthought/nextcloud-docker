#!/bin/bash

# see https://gitlab.com/deepthought/nextcloud-docker
# dependencies should include: docker & docker-compose

# next steps:
#  cleanup unused files
#  add encryption module default-enabled - don't know how yet
#  ensure virtualhost syntax ok re non-letsencrypt - should it be included sans https?

if ! [ $(id -u) = 0 ]; then
   echo "This script must be run with root privileges"
   exit 1
fi

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

CONFIG_NAME="nextcloud-config.sh"
COMPOSE_DEFAULT_NAME="docker-compose.yml.default"

CONFIG="$SCRIPTPATH/$CONFIG_NAME"
COMPOSE_DEFAULT="$SCRIPTPATH/$COMPOSE_DEFAULT_NAME"
COMPOSE="$SCRIPTPATH/docker-compose.yml"
DB_ENV_DEFAULT="$SCRIPTPATH/db.env.default"
DB_ENV="$SCRIPTPATH/db.env"
LETSENCRYPT_FILE_DEFAULT="$SCRIPTPATH/letsencrypt_services.default.yml"
LETSENCRYPT_FILE="$SCRIPTPATH/letsencrypt_services.yml"

REQUIRED_FILES=(
  "$CONFIG"
  "$COMPOSE_DEFAULT"
  "$DB_ENV_DEFAULT"
  "$SCRIPTPATH/app/Dockerfile"
  "$SCRIPTPATH/app/redis.config.php"
  "$SCRIPTPATH/web/Dockerfile"
  "$SCRIPTPATH/web/nginx.conf.nocert"
  "$SCRIPTPATH/web/nginx.conf.letsencrypt"
  "$LETSENCRYPT_FILE_DEFAULT"
)

COMPOSE_START="docker-compose -f ${SCRIPTPATH}/docker-compose.yml up -d"
COMPOSE_DOWN_KILL_VOLS="docker-compose down -v"

REQUIRED_VARS=(
  LOCAL_IP
  CERT_NONE
  CERT_LETSENCRYPT
  DB_MYSQL
  DB_POSTGRES
  PORT_HTTP
)

CERT_LETSENCRYPT_REQUIRED_VARS=(
  DOMAIN_NAME
  LETSENCRYPT_EMAIL
  PORT_HTTPS
)

function zero_or_one {
  if [ $1 -ne 0 ] && [ $1 -ne 1 ]; then
    echo "binary variables in config need to be set only to zero or one - please reload the config or inspect it for mistakes"
    exit 1
  fi
}

for f in "${REQUIRED_FILES[@]}"
do
  if ! [ -f "$f" ]; then
    echo "required file: $f could not be found. please ensure config file & all git files are present.";
    exit 1;
  fi
done

. $CONFIG

SETUP_ONLY=0

read -d '' HALP <<EOL
options:
  no argument: run script for installation
  -r to reset. reset ***will destroy all volume data associated w nextcloud docker***
  -s: setup only. docker-compose will not be called - later have to run: ${COMPOSE_START}
EOL

while getopts "hsdr" opt; do
  case ${opt} in
    h )
      echo "argument -h provided"
      echo "${HALP}"
      exit 0
      ;;
    s )
      echo "s argument: setup only - will not start docker-compose"
      echo "to start nextcloud later with docker compose: ${COMPOSE_START}"
      SETUP_ONLY=1
      ;;
    d )
      echo "d argument: shutting down and killing volumes..."
      ${COMPOSE_DOWN_KILL_VOLS}
      exit 1
      ;;
    r )
      echo "r argument: reset called"
      cd "${SCRIPTPATH}"
      ${COMPOSE_DOWN_KILL_VOLS}
      THIS_USER=$SUDO_USER
      sudo -u $SUDO_USER git --git-dir=${SCRIPTPATH}/.git pull
      RESET_ARG=""
      if [ $SETUP_ONLY -eq 1 ]; then
        RESET_ARG=" -s"
        echo "will run this script again for setup only"
      else
        echo "will run this script again without arguments for normal installation"
      fi
      ${SCRIPT}${RESET_ARG}
      exit 0
      ;;
    \? )
      echo "Invalid Option: -$OPTARG" 1>&2
      EXITING=1
      ;;
  esac
done
echo "end getops"

#ensure required variables set by config
for var in "${REQUIRED_VARS[@]}"
do
  if [[ -z ${!var+x} ]]; then   # indirect expansion here
    echo "required config variable $var is unset - please set this in the config. exiting...";
    exit 1
  fi
done

#ensure cert vars are 0 or 1
zero_or_one $CERT_NONE
zero_or_one $CERT_LETSENCRYPT
zero_or_one $DB_MYSQL
zero_or_one $DB_POSTGRES

CERT_SUM=`expr "$CERT_NONE" + "$CERT_LETSENCRYPT"`
if [ $CERT_SUM -ne 1 ]; then
  echo "one CERT variable, and only one, needs to be set to 1 in config file"
  exit 1
fi

DB_TYPE_SUM=`expr "$DB_MYSQL" + "$DB_POSTGRES"`
if [ $DB_TYPE_SUM -ne 1 ]; then
  echo "one DB_TYPE variable, and only one, needs to be set to 1 in config file (i.e. mysql OR postgres)"
  exit 1
fi

COMPOSE_TO_USE="$COMPOSE_DEFAULT"
NGINX_TO_USE="$SCRIPTPATH/web/nginx.conf.nocert"
if [ $CERT_NONE -eq 1 ]; then
  echo "cert mode: none"
elif [ $CERT_LETSENCRYPT -eq 1 ]; then
  echo "cert mode: letsencrypt / standalone"
  NGINX_TO_USE="$SCRIPTPATH/web/nginx.conf.letsencrypt"
  for var in "${CERT_LETSENCRYPT_REQUIRED_VARS[@]}"
  do
    if [[ -z ${!var+x} ]]; then   # indirect expansion heredocker-compose -f \"${SCRIPTPATH}/docker-compose.yml\" up -d
      echo "required config variable for letsencrypt setup $var is unset - please set this in the config. exiting...";
      exit 1
    fi
  done
fi
cp $NGINX_TO_USE "$SCRIPTPATH/web/nginx.conf"

NEXTCLOUD_ADMIN_PASSWORD=$(openssl rand -base64 12)
DB_ROOT_PASSWORD=$(openssl rand -base64 15)
DB_PASSWORD=$(openssl rand -base64 15)

#if domain set include in trusted domains (note localhost included by nextcloud installer, don't include here)
NEXTCLOUD_TRUSTED_DOMAINS="$LOCAL_IP"
echo "nextcloud trusted domains pre conditional: $NEXTCLOUD_TRUSTED_DOMAINS"
if [ -n "${DOMAIN_NAME+set}" ]; then
  echo "domain is set: $DOMAIN_NAME"
  NEXTCLOUD_TRUSTED_DOMAINS="${DOMAIN_NAME} ${LOCAL_IP}"
else
  echo "domain is not set"
fi
echo "nextcloud trusted domains post conditional: $NEXTCLOUD_TRUSTED_DOMAINS"

#save passwords
PW_DIR="$SCRIPTPATH/passwords_to_save"
mkdir -p "$PW_DIR"
echo "db root password is ${DB_ROOT_PASSOWRD}" >> "$PW_DIR/db_root_password.txt"
echo "db user password is ${DB_PASSWORD}" >> "$PW_DIR/db_user_password.txt"
echo "nextcloud admin password is ${NEXTCLOUD_ADMIN_PASSWORD}" >> "$PW_DIR/nexcloud_admin_password.txt"

#replacements
cp "$COMPOSE_TO_USE" "$COMPOSE"
cp "$DB_ENV_DEFAULT" "$DB_ENV"
cp "$LETSENCRYPT_FILE_DEFAULT" "$LETSENCRYPT_FILE"

if [ $CERT_LETSENCRYPT -eq 1 ]; then
  echo "should be replacing letsencrypt comment blocks in yml now"
  sed -i "s|\[\[PORT_HTTP\]\]|${PORT_HTTP}|" "${LETSENCRYPT_FILE}"
  sed -i "s|\[\[PORT_HTTPS\]\]|${PORT_HTTPS}|" "${LETSENCRYPT_FILE}"
  sed -i "/#\[\[LETSENCRYPT_SERVICES\]\]/r ${LETSENCRYPT_FILE}" "${COMPOSE}"
  sed -i "s|#environment|environment|" "${COMPOSE}"
  sed -i "s|#- VIRTUAL_HOST=|- VIRTUAL_HOST=|" "${COMPOSE}"
  sed -i "s|#- LETSENCRYPT_|- LETSENCRYPT_|g" "${COMPOSE}"
  sed -i "s|#certs|certs|" "${COMPOSE}"
  sed -i "s|#vhost.d|vhost.d|" "${COMPOSE}"
  sed -i "s|#html|html|" "${COMPOSE}"
  sed -i "s|#networks|networks|g" "${COMPOSE}"
  sed -i "s|#- default|- default|g" "${COMPOSE}"
  sed -i "s|#- proxy-tier|- proxy-tier|g" "${COMPOSE}"
  sed -i "s|#proxy-tier|proxy-tier|g" "${COMPOSE}"
else
    sed -i "s|#\[\[NOCERT_PORTS_HEADER\]\]|ports:|" "${COMPOSE}"
    sed -i "s|#\[\[NOCERT_PORTS\]\]|- $PORT_HTTP:80|" "${COMPOSE}"
fi

DB_TYPE="MYSQL"
DB_IMAGE="mariadb"
DB_VOL="/var/lib/mysql"
DB_VAR="DATABASE"
DB_USER="nextcloud"
if [ $DB_MYSQL -eq 1 ]; then
  echo "db mode: mysql"
  sed -i "s|#command: --transaction|command: --transaction|" "${COMPOSE}"
  sed -i "s|#- MYSQL_ROOT_PASSWORD|- MYSQL_ROOT_PASSWORD|" "${COMPOSE}"
  sed -i "s|#\[\[DB_TYPE|\[\[DB_TYPE|" "${DB_ENV}"
  sed -i "s|#- MYSQL_DATABASE|- MYSQL_DATABASE|g" "${COMPOSE}"

elif [ $DB_POSTGRES -eq 1 ]; then
  echo "db mode: postgres"
  DB_TYPE="POSTGRES"
  DB_IMAGE="postgres"
  DB_VOL="/var/lib/postgresql/data"
  DB_VAR="DB"
  DB_USER="pqluser"
  sed -i "s|#\[\[POSTGRES_PORTS_HEADER\]\]|ports:|" "${COMPOSE}"
  sed -i "s|#\[\[POSTGRES_PORTS\]\]|- 5432:5432|" "${COMPOSE}"
  sed -i "s|#- POSTGRES_DB|- POSTGRES_DB|" "${COMPOSE}"
fi

#function replacevar {
#  sed -i "s|\[\[$1\]\]|${$1}|" "$2"
#}

sed -i "s|\[\[NEXTCLOUD_ADMIN_PASSWORD\]\]|${NEXTCLOUD_ADMIN_PASSWORD}|g" "${COMPOSE}"
sed -i "s|\[\[NEXTCLOUD_TRUSTED_DOMAINS\]\]|${NEXTCLOUD_TRUSTED_DOMAINS}|g" "${COMPOSE}"
sed -i "s|\[\[DOMAIN_NAME\]\]|${DOMAIN_NAME}|g" "${COMPOSE}"
sed -i "s|\[\[LETSENCRYPT_EMAIL\]\]|${LETSENCRYPT_EMAIL}|g" "${COMPOSE}"

sed -i "s|\[\[DB_ROOT_PASSWORD\]\]|${DB_ROOT_PASSWORD}|g" "${COMPOSE}"
sed -i "s|\[\[DB_TYPE\]\]|${DB_TYPE}|g" "${COMPOSE}"
sed -i "s|\[\[DB_IMAGE\]\]|${DB_IMAGE}|g" "${COMPOSE}"
sed -i "s|\[\[DB_VOL\]\]|${DB_VOL}|g" "${COMPOSE}"
sed -i "s|\[\[DB_TYPE\]\]|${DB_TYPE}|g" "${DB_ENV}"
sed -i "s|\[\[DB_PASSWORD\]\]|${DB_PASSWORD}|g" "${DB_ENV}"
sed -i "s|\[\[DB_VAR\]\]|${DB_VAR}|g" "${DB_ENV}"
sed -i "s|\[\[DB_VAR\]\]|${DB_VAR}|g" "${DB_ENV}"
sed -i "s|\[\[DB_USER\]\]|${DB_USER}|g" "${DB_ENV}"

# need to figure out how to pre-install/enable encryption
#iocage exec ${JAIL_NAME} su -m www -c 'php /usr/local/www/apache24/data/nextcloud/occ app:enable encryption'
#iocage exec ${JAIL_NAME} su -m www -c 'php /usr/local/www/apache24/data/nextcloud/occ encryption:enable'

docker-compose -f "${SCRIPTPATH}/docker-compose.yml" pull
docker-compose -f "${SCRIPTPATH}/docker-compose.yml" build
if [ $SETUP_ONLY -ne 1 ]; then
  ${COMPOSE_START}
  echo "note: it may take a minute for the web server to start up"
else
  echo "note to start nextcloud docker will have to run: ${COMPOSE_START}"
fi

echo "nextcloud admin user password is: $NEXTCLOUD_ADMIN_PASSWORD"
